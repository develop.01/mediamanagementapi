﻿using AutoMapper;
using MediaManagementApi.Model;
using MediaManagementApi.Model.MediaManagementDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediaManagementApi.Service
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile() {

            CreateMap<FileMedia, FileMediaModel>().ReverseMap();
        }
     
    }
}
