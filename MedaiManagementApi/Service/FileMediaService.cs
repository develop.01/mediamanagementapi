﻿using Microsoft.Extensions.Configuration;
using MediaManagementApi.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using Microsoft.EntityFrameworkCore.Storage;

namespace MediaManagementApi.Service
{
    public class FileMediaService
    {
        private readonly MediaDBContext _db;
        private readonly IConfiguration _configuration;

        public FileMediaService(MediaDBContext db, IConfiguration configuration)
        {
            this._db = db;
            this._configuration = configuration;
        }

        public FileMediaListModel GetListFileMedia()
        {
            try
            {
                var result = new FileMediaListModel();
                var fileList = (from t in _db.FileMedia
                                where t.IsActive == true
                                select new FileMediaModel
                                {
                                    FileId = t.FileId,
                                    FileName = t.FileName,
                                    FilePath = t.FilePath,
                                    FileLength = t.FileLength
                                }).ToList();
                result.FileMediaModels = fileList;
                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<bool> UploadFile(FileUploadModel model)
        {
            try
            {
                var File = model.File;
                foreach (var file in File)
                {
                    Guid id = Guid.NewGuid();
                    //string fileNameBase = file.FileName + "_" + id;
                    string fileType = Path.GetExtension(file.FileName);
                    string fileName = Path.GetFileNameWithoutExtension(file.FileName) + "_" + id + fileType;
                    var destPath = _configuration.GetSection("Path:FileUploadPath").Value;
                    var destFolder = _configuration.GetSection("Path:Folder").Value;
                    var path = Path.Combine(destPath, destFolder, fileName);
                    var itemUpdate = _db.FileMedia
                            .Where(w => w.FileName == file.FileName && w.IsActive == true).FirstOrDefault();

                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        file.CopyToAsync(stream).GetAwaiter().GetResult();
                        var uploadFile = new Model.MediaManagementDB.FileMedia
                        {

                            FilePath = path,
                            FileName = file.FileName,
                            FileLength = file.Length,
                            CreateDate = DateTime.Now,
                            UpdateDate = DateTime.Now,
                            CreateBy = "",
                            UpdateBy = "",
                            IsActive = true

                        };
                        _db.FileMedia.Add(uploadFile);

                    }
                }

                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;

            }
        }

        public async Task<bool> UpdateFileName(FileUpdateModel model)
        {
            bool res = false;
            using (IDbContextTransaction transaction = _db.Database.BeginTransaction())
            {
                try
                {
                    var itemExists = _db.FileMedia.FirstOrDefault(w => w.FileId == model.FileId && w.IsActive == true);

                    if (itemExists != null)
                    {
                        itemExists.FileName = model.FileName;
                        itemExists.UpdateDate = DateTime.Now;
                        itemExists.UpdateBy = "";
                        _db.SaveChanges();
                    }
                    transaction.Commit();
                    res = true;
                }
                catch (Exception ex)
                {

                    transaction.Rollback();
                    res = false;
                }

            }
            return res;


        }
        public async Task<bool> DeleteFile(int fileId)
        {
            bool res = false;
            using (IDbContextTransaction transaction = _db.Database.BeginTransaction())
            {
                try
                {
                    var itemExists = _db.FileMedia.FirstOrDefault(w => w.FileId == fileId && w.IsActive == true);

                    if (itemExists != null)
                    {
                        itemExists.IsActive = false;
                        itemExists.UpdateDate = DateTime.Now;
                        itemExists.UpdateBy = "";
                        _db.SaveChanges();
                    }
                    transaction.Commit();
                    res = true;
                }
                catch (Exception ex)
                {

                    transaction.Rollback();
                    res = false;
                }
                
            }
            return res;


        }
    }
}
