﻿using MediaManagementApi.Model;
using MediaManagementApi.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MediaManagementApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FileMediaController : ControllerBase
    {
        private FileMediaService _FileMediaService;
        public FileMediaController(FileMediaService FileMediaService)
        {
            this._FileMediaService = FileMediaService;
        }
            
      
        [HttpGet("GetListFileMedia")]
        public FileMediaListModel GetListFileMedia()
        {
          
            var result = _FileMediaService.GetListFileMedia();
            return result;
        }

        [HttpPost("UploadFile")]
        public  bool UploadFile([FromForm] FileUploadModel model)
        {
            try
            {
                var result = _FileMediaService.UploadFile(model).GetAwaiter().GetResult();
                return true;

            }
            catch (Exception ex)
            {
                return false;
                
            }

        }

        [HttpPost("UpdateFileName")]
        public bool UpdateFileName(FileUpdateModel model)
        {
            try
            {
                var result = _FileMediaService.UpdateFileName(model).GetAwaiter().GetResult();
                return true;

            }
            catch (Exception ex)
            {

                return false;
            }
        }
        [HttpPost("DeleteFile")]
        public bool DeleteFile(int fileId) {
            try
            {
                var result = _FileMediaService.DeleteFile(fileId).GetAwaiter().GetResult();
                return true;

            }
            catch (Exception ex)
            {

                return false;
            }
        }
    }
}
