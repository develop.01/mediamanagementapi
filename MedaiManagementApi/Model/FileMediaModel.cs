﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediaManagementApi.Model
{
    public class FileMediaListModel { 
        public IEnumerable<FileMediaModel> FileMediaModels { get; set; }
    }
    public class FileMediaModel
    {
        public int FileId { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public long FileLength { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string CreateBy { get; set; }
        public string UpdateBy { get; set; }
        public bool IsActive { get; set; }
    }
    public class FileUploadModel
    {
     
        public IEnumerable<IFormFile> File { get; set; }
    }

    public class FileUpdateModel {
        public int FileId { get; set; }
        public string FileName { get; set; }
    }
}
