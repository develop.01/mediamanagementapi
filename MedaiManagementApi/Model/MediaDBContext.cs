﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace MediaManagementApi.Model
{
    public class MediaDBContext : DbContext
    {
        public MediaDBContext(DbContextOptions<MediaDBContext> options) : base(options)
        { }
        public DbSet<MediaManagementDB.FileMedia> FileMedia { get; set; }
        public DbSet<MediaManagementDB.ExtensionType> ExtensionType { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
