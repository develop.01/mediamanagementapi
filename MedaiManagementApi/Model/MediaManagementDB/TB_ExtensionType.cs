﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MediaManagementApi.Model.MediaManagementDB
{
    [Table("ExtensionType")]
    public class ExtensionType
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ExtensionTypeId { get; set; }
        public string ExtensionTypeDesc { get; set; }
        public bool IsActive { get; set; }
    }
}
